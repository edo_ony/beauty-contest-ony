Dropzone.autoDiscover = false;
var previewNode = document.querySelector("#dztemplate");
previewNode.id = "";
var previewTemplate = previewNode.parentNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);

let myDropzone = new Dropzone("div#dropzone", {
    url: function (files) {
        console.log(files);
        return "/file/upload"
    },
    headers: {
        'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
    },
    accept: function(file, done) {
        var thumbnail = $(file.previewElement).find('.img-preview');
        var img = $(file.previewElement).find('.img-preview > img');

        switch (file.type) {
          case 'application/pdf':
            thumbnail.css('background', 'url(assets/img/pdf-icon.png)');
            img.css('display', 'none')
            break;
          case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
            thumbnail.css('background', 'url(img/doc.png');
            img.css('display', 'none')
            break;
          case 'application/x-zip-compressed':
            thumbnail.css('background', 'url(img/doc.png');
            img.css('display', 'none')
            break;
        }
        const size = myDropzone.filesize(file.size)
        $(file.previewElement).find('[data-dz-name]').text(file.upload.filename)
        $(file.previewElement).find('[data-dz-size]').html(size)

        done();
    },
    autoQueue: false,
    autoProcessQueue: false,
    paramName: "file",
    hiddenInputContainer: '#dzpreview',
    previewTemplate: previewTemplate,
    previewsContainer: '#dzpreview'
});
myDropzone.on("error", (file, message) => {
    myDropzone.removeFile(file);
    if (file.previewElement) {
        file.previewElement.classList.add("dz-error");
        if (typeof message !== "string" && message.errors.file) {
            message = message.errors.file.join(', ');
            $(myDropzone.options.previewsContainer).prepend('<div class="file-error"><span>'+message+'</span></div>')
        }
    }
});
