let medias = [], deletedMedias = [], deletedVariants = []
let swal
if (typeof Swal !== 'undefined') {
    swal = Swal.mixin({
            reverseButtons: true,
            showLoaderOnConfirm: true
        })
}
$(function(){

    // input password
    $('.--show-pass').on('click', function(e){
        e.preventDefault()
        const $btn = $(this), fa = $btn.find('.fa')
        fa.toggleClass('fa-eye-slash')
        const $pass = $btn.parents('.input-group').find('.--input-pass');
        if($pass.attr('type') === 'password') {
            $pass.attr('type', 'text')
        } else [
            $pass.attr('type', 'password')
        ]
    })

    // datepicker
    if ($('.datepicker').length) {
        $('.datepicker').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            autoApply: true,
            minYear: 2000,
            maxDate: moment().format('MM/DD/YYYY'),
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });
        $('input.datepicker').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('MM/DD/YYYY'));
        });
        $('input.datepicker').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    }
    if ($('.daterange').length) {
        $('.daterange').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            },
            autoApply: true,
            minYear: 2000,
            maxDate: moment().format('MM/DD/YYYY')
        });
        $('input.daterange').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        });
        $('input.daterange').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    }

    // image/file browse
    $('.--file-browse').on('change', function(e){
        var files = e.target.files
        const file = files[0]
        const mime = file.type
        const [type, format] = mime.split('/')
        $('#file_mime').val(mime)
        console.log(type, format);
        const $thumb = $(this).siblings('.source')
        const reader = new FileReader();
        reader.addEventListener('load', (event) => {
            if (type === 'image') {
                $thumb.html('<img src="'+event.target.result+'" class="img-thumbnail">')
            } else if(type === 'video') {
                $thumb.html('<video controls><source src="'+event.target.result+'" type="'+file.type+'"></video>')
            }
        });
        reader.readAsDataURL(file);
        $('.--clear-browse').removeClass('d-none');
    })
    $('.--clear-browse').on('click', function(e){
        e.preventDefault()
        const target = $(this).data('for')
        const origin = $(this).data('origin')
        const $thumb = $(target).siblings('.source')
        if (typeof origin !== 'undefined') {
            $thumb.html('')
        } else {
            $thumb.html('')
        }
        $(target).val('')
        $('.--clear-browse').addClass('d-none');
    })

    // TinyMCE
    if($('.rich-text').length) {
        tinymce.init({
            selector: '.rich-text',
            plugins: [
                'advlist', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview',
                'anchor', 'searchreplace', 'visualblocks', 'code', 'fullscreen',
                'insertdatetime', 'media', 'table', 'help', 'wordcount'
            ],
            menubar: false,
            toolbar: 'undo redo | blocks | ' +
            'bold italic backcolor | alignleft aligncenter ' +
            'alignright alignjustify | bullist numlist outdent indent | ' +
            'media image | removeformat',
            content_style: 'body { background-color: #f3f3f3; }',
            setup: function(editor) {
                editor.on('focus', function(e) {
                    editor.getBody().style.background = '#FFF';
                });
                editor.on('blur', function(e) {
                    editor.getBody().style.background = '#f3f3f3';
                });
              }
        });
    }

    // select2
    $('select').each(function() {
        let minimumResultsForSearch = Infinity
        let dropdownParent = $(this).data('dropdown-parent') || 'body'
        if ($(this).data('allow-search')) {
            minimumResultsForSearch = 3
        }
        if ($(this).data('placeholder')) {
            $(this).select2({
                minimumResultsForSearch,
                allowClear: true,
                dropdownParent
            })
        } else {
            $(this).select2({
                minimumResultsForSearch,
                dropdownParent
            })
        }
    })

    $('.navbar-toggler').on('click', function(e){
        e.preventDefault();
        $('.sidebar').addClass('show')
        $('.body-content').append('<div class="sidebar-blanket"></div>')
    })

    var maximize = sessionStorage.getItem('maximize')
    $('.navbar-toggler-lg').on('click', function(e){
        e.preventDefault();
        $('.body-content').toggleClass('maximize')
        if (maximize) {
            sessionStorage.removeItem('maximize')
        } else {
            sessionStorage.setItem('maximize', true)
        }
    })
    if (maximize) {
        $('.body-content').addClass('maximize')
    }

    $('.sidebar-toggler > .btn-close').on('click', function(e){
        e.preventDefault();
        $('.sidebar').removeClass('show')
        $('.sidebar-blanket').remove()
    })
    $('.page').css({ transition: 'width .5s, margin .5s'})

    $('.search-toggler').on('click', function(e){
        e.preventDefault();
        $('.form-search').addClass('show')
    })
    $('.header-search-toggle > .btn-close').on('click', function(e){
        e.preventDefault();
        $('.form-search').removeClass('show')
    })

    $(document).on('click', '.check-all', function(){
        const check = $('.check-all').prop('checked')
        $('.check-row').prop('checked', check)
        $('.bulk-action select').prop('disabled', !check)
        const lenCheck = $('.check-row:checked').length
        if (lenCheck <= 0) {
            $('.bulk-action select').val(null).trigger('change')
            $('.bulk-action').hide()
        } else {
            $('.bulk-action').show()
        }
        $('.checked-count').text(lenCheck)
    })
    $(document).on('click', '.check-row', function(){
        const len = $('.check-row').length
        const lenCheck = $('.check-row:checked').length
        if (lenCheck <= 0) {
            $('.bulk-action select').val(null).trigger('change')
            $('.bulk-action').hide()
        } else {
            $('.bulk-action').show()
        }
        $('.check-all').prop('checked', lenCheck > 0)
        $('.check-all').prop('indeterminate', lenCheck > 0 && lenCheck!==len)
        $('.bulk-action select').prop('disabled', !(lenCheck > 0))
        $('.checked-count').text(lenCheck)
    })
    $('.bulk-action select').on('change', function (){
        const val = $(this).val()
        $('.bulk-action .btn').prop('disabled', val.trim()==='')
    })
    $('.--btn-bulk').on('click', function(e){
        e.preventDefault()
        const items = $('.check-row').serialize(),
              type = $('.bulk-action select').val(),
              lenCheck = $('.check-row:checked').length,
              action = $('.bulk-action select > option:selected').data('action')
            console.log(items);
        if (type === 'delete') {
            swal.fire({
                title: `Menghapus ${lenCheck} data?`,
                text: `Anda tidak dapat memulihkan data ini!`,
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, hapus data",
                cancelButtonText: "Batal",
                preConfirm: () => {
                    $.ajax({
                        url : action,
                        method : 'post',
                        data: items,
                        headers : {'X-CSRF-TOKEN': $('[name="_token"]').val()},
                        success : function (res) {
                            swal.fire({
                                title: res.message.title,
                                text: res.message.desc,
                                icon: res.message.type,
                                showConfirmButton: false,
                                timer: 3000,
                                showLoaderOnConfirm: false,
                                didClose: () => {
                                    if (res.redirect) {
                                        window.location = res.redirect;
                                    }
                                    if (res.reload) {
                                        window.location.reload();
                                    }
                                }
                            });
                        }
                    });
                }
            })
        } else if (type === 'archive') {
            swal.fire({
                title: `Archive ${lenCheck} data?`,
                text: `Anda dapat memulihkan data ini`,
                icon: "question",
                showCancelButton: true,
                confirmButtonColor: "#1dafd5",
                confirmButtonText: "Ya, archive data",
                cancelButtonText: "Batal",
                preConfirm: () => {}
            })
        }

    })


    // FORM SUBMIT
    $('form:not(.has-dropzone):not(#form-filter)').on('submit', function(e){
        e.preventDefault();
        var form = $(this), action = form.attr('action'), formData = new FormData(form[0]);
        var $btn = $(this).find('[type="submit"]')
        $('.is-invalid').removeClass('is-invalid');
        $('.invalid-feedback').remove();
        if (medias.length) {
            medias.forEach((val, key) => {
                formData.append('medias['+key+']', val);
            });
        }
        if(deletedMedias.length){
            deletedMedias.forEach((val, key) => {
                formData.append('deletedMedias['+key+']', val);
            });
        }
        if(deletedVariants.length){
            deletedVariants.forEach((val, key) => {
                formData.append('deletedVariants['+key+']', val);
            });
        }
        if (form.find('.ckeditor').length) {
            form.find('.ckeditor').each(function(){
                var id = $(this).attr('id'), name = $(this).attr('name');
                console.log(CKEDITOR.instances[id].getData());
                formData.set(name, CKEDITOR.instances[id].getData());
            })
        }
        $('.form-loading').remove()
        $.ajax({
            url: action,
            data: formData,
            type: 'post',
            contentType: false,
            processData: false,
            beforeSend: function () {
                if (form.hasClass('form-modal')) {
                    const $modal = form.parents('.modal-content')
                    $modal.append('<div class="form-loading"><div class="m-auto"><i class="fi fi-rr-loading text-light spin fs-2"></i></div></div>')
                } else {
                    let wrapper = $btn.parents('.has-loading')
                    if (wrapper.length) {
                        wrapper.append('<div class="form-loading"><div class="m-auto"><i class="fi fi-rr-loading text-light spin fs-2"></i></div></div>')
                    } else {
                        $('body').append('<div class="form-loading fixed"><div class="m-auto"><i class="fi fi-rr-loading text-light spin fs-2"></i></div></div>')
                    }
                }
            },
            success: function (res) {
                $('.form-loading').remove()
                if (res.dom) {
                    $('#'+res.dom.id).html(res.dom.content)
                }
                if (res.error) {
                    swal.fire({
                        toast: true,
                        title: res.error,
                        icon: 'error',
                        showConfirmButton: false,
                        showLoaderOnConfirm: false,
                        timer: 3000,
                        didClose: () => {}
                    });
                } else {
                    if (res.alert) {
                        swal.fire({
                            toast: true,
                            title: res.alert.title,
                            text: res.alert.desc,
                            icon: res.alert.type,
                            showConfirmButton: false,
                            showLoaderOnConfirm: false,
                            timer: 3000,
                            didClose: () => {
                                if (res.redirect) {
                                    window.location = res.redirect;
                                } else if (res.reset){
                                    form[0].reset()
                                }
                            }
                        });
                    } else if (res.message) {
                        swal.fire({
                            title: res.message.title,
                            text: res.message.desc,
                            html: res.message.html,
                            icon: res.message.type
                        }).then((result) => {
                            if (result.isConfirmed) {
                                if (res.redirect) {
                                    window.location = res.redirect;
                                } else if (res.reset){
                                    form[0].reset()
                                }
                            }
                          })
                    } else {
                        if(res.redirect) {
                            window.location = res.redirect;
                        }
                    }
                }
            },
            error: function (res) {
                $('.form-loading').remove()

                $.each(res.responseJSON.errors, function(field, msg) {
                    var fieldname = '[name="'+field+'"],[name="'+field+'[]"]';
                    $(fieldname).addClass('is-invalid');
                    const parent = $(fieldname).parent()
                    if (parent.hasClass('input-group')) {
                        parent.after('<div class="invalid-feedback">'+msg.join(", ")+'</div>');
                    } else {
                        parent.append('<div class="invalid-feedback">'+msg.join(", ")+'</div>');
                    }
                    // $(fieldname).parents('.float-inpt-fld').addClass('is-invalid');
                    // $(fieldname).parents('.multple-selects2').addClass('is-invalid');
                    // if ($('[name="'+field+'"]').parent('.browse-wrapper').length > 0) {
                    //     $('[name="'+field+'"]').parent('.browse-wrapper').append('<div class="invalid-feedback">'+msg.join(", ")+'</div>');
                    // } else {
                    // }
                })
            }
        })
    });

    var href;
    function reloadTable(url, offset){
        href = url || window.location.href;
        const dataForm = $('#form-filter').serialize()
        $.ajax({
            url: href,
            data: dataForm,
            success: function(res) {
                if(res.section) {
                    $('#'+res.section).html(res.html);
                } else {
                    $('#dataTable').html(res.html);
                }
                $('.bulk-action').hide()
                $('.select-entries > select').select2({
                    minimumResultsForSearch: Infinity,
                })
            }
        })

        return false
    }
    $('form#form-filter').on('submit', function(e){
        e.preventDefault()
        reloadTable()
    })
    $(document).on('click', 'a.page-link', function(e){
        e.preventDefault();
        var $btn = $(this);
        var href = $btn.attr('href')
        reloadTable(href)
    });
    $(document).on('change', '.select-entries > select', function(e) {
        e.preventDefault();
        const href = window.location.href
        const perpage = $(this).val()
        $('.select-entries > select').val(perpage)
        reloadTable(href, perpage)
    })

    $('.--btn-del').on('click', function(e){
        e.preventDefault()
        var action = $(this).attr('href');
        swal.fire({
            title: "Apakah Anda yakin?",
            text: "Anda tidak dapat memulihkan data ini!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, hapus data",
            cancelButtonText: "Batal",
            preConfirm: () => {
                $.ajax({
                    url : action,
                    method : 'post',
                    headers : {'X-CSRF-TOKEN': $('[name="_token"]').val()},
                    success : function (res) {
                        swal.fire({
                            toast: true,
                            title: res.message.title,
                            text: res.message.desc,
                            icon: res.message.type,
                            showConfirmButton: false,
                            timer: 3000,
                        }, function (isDismissed) {
                            if (res.redirect) {
                                window.location = res.redirect;
                            }
                            if (res.reload) {
                                window.location.reload();
                            }
                        });
                    }
                });
            }
        });
    })


    $('.--rm-single-img').on('click', function(e) {
        e.preventDefault();
        const $btn = $(this)
        const action = $btn.data('action');
        $.ajax({
            url: action,
            type: 'post',
            headers : {'X-CSRF-TOKEN': $('[name="_token"]').val()},
            success: function (res) {
                if (res.success) {
                    const target = $btn.data('for')
                    const $thumb = $(target).siblings('.source')
                    $thumb.html('')
                    $(target).val('')
                    $btn.addClass('d-none')
                    swal.fire({
                        toast: true,
                        title: res.message.title,
                        text: res.message.desc,
                        icon: res.message.type,
                        showConfirmButton: false,
                        showLoaderOnConfirm: false,
                        timer: 3000,
                        didClose: () => {}
                    });
                }
            }
        })
    })

    $(document).on('change', '.switch-status', function(){
        const action = $(this).data('action');
        console.log(action);
        $.ajax({
            url: action,
            type: 'post',
            headers : {'X-CSRF-TOKEN': $('[name="_token"]').val()},
            success: function(res) {
                swal.fire({
                    toast: true,
                    title: res.message.title,
                    text: res.message.desc,
                    icon: res.message.type,
                    iconColor: 'white',
                    showConfirmButton: false,
                    timer: 3000,
                    showLoaderOnConfirm: false,
                    position: 'top',
                    customClass: {
                        popup: 'success-toast'
                    }
                });
            },
            error: function (res) {
                swal.fire({
                    toast: true,
                    title: res.message.title,
                    text: res.message.desc,
                    icon: res.message.type,
                    iconColor: 'white',
                    showConfirmButton: false,
                    timer: 3000,
                    showLoaderOnConfirm: false,
                    position: 'top'
                });
            }
        })
    });

})
