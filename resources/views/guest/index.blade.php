@extends('layout-parking')

@section('content')
<!-- nav -->
  <nav class="parking-navbar">
    <a href="">
      <div class="logo">

      </div>
    </a>
    @auth
    <div class="user-setting">
      <div class="user-display"></div>
      <ul class="user-menu" style="display: none">
        <li>
          <a href="{{route('logout')}}">
            Logout
          </a>
        </li>
      </ul>
    </div>
    @endauth
  </nav>
  <!-- end nav -->
  <section class="content">
    <!-- slot-parkir -->
    <div class="slot-parkir">
      <div class="slot-parkir-2">
        <div class="slot-parkir-box">
          <div class="title">Sisa Slot Parkir Roda 2</div>
          <div class="value" id="slotR2">{{$availableR2}}</div>
        </div>
        <div class="text">Tarif Parkir : Rp {{$ratesR2}}/Jam</div>
      </div>
      <div class="slot-parkir-4">
        <div class="slot-parkir-box">
          <div class="title">Sisa Slot Parkir Roda 4</div>
          <div class="value" id="slotR4">{{$availableR4}}</div>
        </div>
        <div class="text">Tarif Parkir : Rp {{$ratesR4}}/Jam</div>
      </div>
    </div>
    <!-- end-slot-parkir -->
    <!-- parking-info -->
    <div class="parking-info">
      <div class="clock">10:00</div>
      <div class="date">{{now()->format('d F Y')}}</div>
      {{-- <div class="location">Parkiran Hotel Bumijo Yogyakarta</div> --}}
    </div>
    <!-- end parking-info -->
    <!-- footer -->
  </section>
  <footer class="parking-footer">
    <div class="footer-title">ATURAN PARKIR</div>
    <div class="footer-desc">Lorem ipsum dolor sit amet consectetur. A cursus suspendisse fermentum egestas ullamcorper adipiscing quis at. Cursus metus lacus vehicula felis tortor. Porttitor elit convallis commodo tincidunt viverra.</div>
  </footer>
  <!-- end footer -->
@endsection

@section('script')
<script>
    $('.user-display').on('click', ()=> {
        $('.user-menu').slideToggle()
    })

    if ($('.clock').length) {
        $('.clock').html(moment().format('H:mm'))
        setInterval(() => {
            $('.clock').html(moment().format('H:mm'))
        }, 1000);

        setInterval(() => {
            $.ajax({
                url: '{{ route("guests.slot") }}',
                success: function(res) {
                    $('#slotR2').html(res.data.slotAvailableR2)
                    $('#slotR4').html(res.data.slotAvailableR4)
                }
            })
        }, 5000);
    }

</script>
@endsection
