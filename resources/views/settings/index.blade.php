@extends('layout')

@section('content')
<div class="page-title">
    <div class="row d-flex justify-content-between align-items-center">
        <div class="col">
            <h3 class="mb-0">Settings</h3>
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="#">Settings</a></li>
            </ol>
        </div>
    </div>
</div>

<div class="page-content">
    <form action="{{ route('settings.update') }}">
    @csrf
    <div class="row row-wrapper">
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <h5>Kendaraan Roda Dua (R2)</h5>
                    <div class="row mb-3">
                        <div class="col">
                            <div class="mb-3">
                                <label for="name" class="form-label">Kapasitas R2</label>
                                <div class="input-group">
                                    <input type="number" name="settings[capacity_r2]" id="capacity_r2" class="form-control" placeholder="Masukkan kapasitas parkir R2 ..." value="{{ $data->capacity_r2 ?? '' }}">
                                    <span class="input-group-text">slot</span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="mb-3">
                                <label for="name" class="form-label">Tarif R2</label>
                                <div class="input-group">
                                    <span class="input-group-text">Rp</span>
                                    <input type="number" name="settings[rates_r2]" id="rates_r2" class="form-control" placeholder="Masukkan tarif parkir R2 ..." value="{{ $data->rates_r2 ?? '' }}">
                                    <span class="input-group-text">/jam</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h5>Kendaraan Roda Empat (R4)</h5>
                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label for="name" class="form-label">Kapasitas R4</label>
                                <div class="input-group">
                                    <input type="number" name="settings[capacity_r4]" id="capacity_r4" class="form-control" placeholder="Masukkan kapasitas parkir R4 ..." value="{{ $data->capacity_r4 ?? '' }}">
                                    <span class="input-group-text">slot</span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="mb-3">
                                <label for="name" class="form-label">Tarif R4</label>
                                <div class="input-group">
                                    <span class="input-group-text">Rp</span>
                                        <input type="number" name="settings[rates_r4]" id="rates_r4" class="form-control" placeholder="Masukkan tarif parkir R4 ..." value="{{ $data->rates_r4 ?? '' }}">
                                        <span class="input-group-text">/jam</span>
                                    </div>
                            </div>
                        </div>
                    </div>
                    @if(auth()->user()->can('save.settings'))
                    <div class="row d-flex justify-content-end">
                        <div class="col-auto">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    </form>
</div>


@endsection
