<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OnPark</title>
    <link rel="icon" type="image/x-icon" href="/assets/img/dummy-logo.png">
    <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/uicons/css/uicons.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('assets/sweetalert/sweetalert.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/daterangepicker/daterangepicker.css') }}" type="text/css" />
    @yield('style-package')
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    @yield('style')
</head>
<body>
    <div class="content py-5">
        @yield('content')
        <div class="row gx-0 d-flex justify-content-center align-items-center mt-5">
            <div class="col-auto mt-5">
                <a href="{{route('dashboard')}}" class="btn btn-link btn-sm">Dashboard</a>
                <span>|</span>
                <a href="{{route('logout')}}" class="btn btn-link btn-sm">Logout</a>
            </div>
        </div>
    </div>
    <div class="footer fixed-bottom">
        <div class="row">
            <div class="col">
                <p class="mb-0">
                    Copyright © 2023 Designed by <a href="https://oninyon.com" target="_blank" class="colored">Oninyon</a>
                </p>
            </div>
        </div>
    </div>
    <script src="{{asset('assets/js/jquery-3.6.3.min.js')}}"></script>
    <script src="{{asset('assets/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/momentjs/moment.js')}}"></script>
    <script src="{{asset('assets/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/sweetalert/sweetalert.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="{{asset('assets/js/app.js')}}"></script>
    <script>
        if ($('.time-real').length) {
            $('.time-real').html(moment().format('DD MMMM YYYY H:mm:ss'))
            setInterval(() => {
                $('.time-real').html(moment().format('DD MMMM YYYY H:mm:ss'))
            }, 1000);
        }
    </script>
    @yield('script')
</body>
</html>
