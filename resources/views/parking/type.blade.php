@extends('layout-staff')

@section('content')
<div class="page-content">
    <div class="row gx-0">
        <div class="col-md-4 m-auto">
            <h4 class="text-center mb-3">Parkir {{ucfirst($modeLabel)}} {{ ucfirst($type) }}</h4>
            <div class="card mb-3">
                <div class="card-body">
                    <p class="text-center">
                        <small class="time-real"></small>
                    </p>
                    <form action="{{route('parking.' . $mode, ['type' => $type])}}">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <input type="text" name="nomor_plat" id="" class="form-control form-control-lg text-center" placeholder="Input Nomor Plat Kendaraan">
                                    <div class="form-text">Format nomor plat kendaraan: AX&lt;spasi&gt;1234&lt;spasi&gt;X</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="mb-3">
                                    <button class="btn btn-primary w-100">Simpan</button>
                                </div>
                                <div class="text-center my-3"><strong>Kapasitas <span id="slotCounter">{{$countParked}}</span>/{{$capacity->value}}</strong></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row gx-0 d-flex justify-content-end">
                <div class="col-auto px-3">
                    <a href="{{route('parking.mode', ['mode'=>$mode])}}" class="btn btn-secondary">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $('.time-real').html(moment().format('DD MMMM YYYY H:mm:ss'))
    setInterval(() => {
        $('.time-real').html(moment().format('DD MMMM YYYY H:mm:ss'))
    }, 1000);
    $('[name="nomor_plat"]').on('keyup', function () {
        let val = $(this).val();
        $(this).val(val.toUpperCase())
    })
</script>
@endsection
