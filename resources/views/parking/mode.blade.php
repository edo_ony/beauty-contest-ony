@extends('layout-staff')

@section('content')
<div class="page-content">
    <div class="row gx-0">
        <div class="col-md-4 m-auto">
            <h4 class="text-center mb-3">Parkir {{ $modeLabel }}</h4>
            <div class="card mb-3">
                <div class="card-body">
                    <p class="text-center">
                        <small class="time-real"></small>
                    </p>
                    <h5 class="text-center mb-3">Pilih Jenis Kendaraan</h5>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <a class="btn btn-lg btn-primary w-100" href="{{route('parking.type',['type'=>'r2', 'mode' => $mode])}}">R2</a>
                        </div>
                        <div class="col-md-6">
                            <a class="btn btn-lg btn-success w-100" href="{{route('parking.type',['type'=>'r4', 'mode' => $mode])}}">R4</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-0 d-flex justify-content-end">
                <div class="col-auto px-3">
                    <a href="{{route('parking.index')}}" class="btn btn-secondary">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
