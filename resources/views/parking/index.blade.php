@extends('layout-staff')

@section('content')
<div class="page-content">
    <div class="row gx-0">
        <div class="col-md-4 m-auto">
            <div class="card">
                <div class="card-body">
                    <p class="text-center">
                        <small class="time-real"></small>
                    </p>
                    <h4 class="text-center mb-3">Pilih Jenis Pelayanan</h4>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <a class="btn btn-lg btn-primary w-100" href="{{route('parking.mode',['mode'=>'checkin'])}}">Masuk</a>
                        </div>
                        <div class="col-md-6">
                            <a class="btn btn-lg btn-success w-100" href="{{route('parking.mode',['mode'=>'checkout'])}}">Keluar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
