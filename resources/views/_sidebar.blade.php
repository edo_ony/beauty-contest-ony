<div>
    <div class="brand d-flex justify-content-center align-items-center">
        <div class="brand-content">
            <img src="{{ asset('assets/img/logo-dark.png')}}" alt="" class="img-responsive">
        </div>
    </div>
    <div class="mt-4">
        <div class="sidebar-menu">
            <span class="sidebar-toggler">
                <button type="button" class="btn-close"></button>
            </span>
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="{{route('dashboard')}}" class="nav-link {{request()->is('/')?'active':''}}"><i class="fi fi-rr-dashboard"></i><span>Dashboard</span></a>
                </li>
                @if(auth()->user()->can('user'))
                <li class="nav-item has-submenu">
                    <a class="nav-link {{request()->is(['user','user/*'])?'active':''}}" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"><i class="fi fi-rr-user"></i><span>User</span></a>
                    <ul class="nav flex-column collapse {{request()->is(['user','user/*'])?'show':''}}" id="collapseExample">
                        <li class="nav-item">
                            <a href="{{route('user.index')}}" class="nav-link {{request()->is('user')?'active':''}}"><span>User List</span></a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('user.create')}}" class="nav-link {{request()->is('user/create')?'active':''}}"><span>Add New</span></a>
                        </li>
                    </ul>
                </li>
                @endif
                @if(auth()->user()->can('role'))
                <li class="nav-item has-submenu">
                    <a class="nav-link {{request()->is(['role','role/*'])?'active':''}}" data-bs-toggle="collapse" href="#roleCollapse" role="button" aria-expanded="false" aria-controls="roleCollapse"><i class="fi fi-rr-user"></i><span>Role</span></a>
                    <ul class="nav flex-column collapse {{request()->is(['role','role/*'])?'show':''}}" id="roleCollapse">
                        <li class="nav-item">
                            <a href="{{route('role.index')}}" class="nav-link {{request()->is('role')?'active':''}}"><span>Role List</span></a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('role.create')}}" class="nav-link {{request()->is('role/create')?'active':''}}"><span>Add New</span></a>
                        </li>
                    </ul>
                </li>
                @endif
                @if(auth()->user()->can('parking'))
                <li class="nav-item">
                    <a href="{{route('parking.index')}}" class="nav-link {{request()->is(['parking','parking/*'])?'active':''}}"><i class="fi fi-rr-car"></i><span>Parking</span></a>
                </li>
                @endif
                @if(auth()->user()->can('settings') || auth()->user()->can('view.settings'))
                <li class="nav-item">
                    <a href="{{route('settings.index')}}" class="nav-link {{request()->is(['settings'])?'active':''}}"><i class="fi fi-rr-gears"></i><span>Settings</span></a>
                </li>
                @endif
              </ul>
        </div>
    </div>
</div>
