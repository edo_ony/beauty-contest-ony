<div class="footer">
    <div class="row">
        <div class="col">
            <p class="mb-0">
                Copyright © 2023 Designed by <a href="https://oninyon.com" target="_blank" class="colored">Oninyon</a>
            </p>
        </div>
    </div>
</div>
