<label class="label-wrap ms-2 select-entries">
    <select name="perpage" form="form-filter" class="form-select form-select-sm select2-infinity" aria-label="Show per page" data-width="120">
        @foreach ($entries as $value)
        <option value="{{$value}}" {{ $perpage==$value?'selected':'' }}>{{$value}} / page</option>
        @endforeach
    </select>
</label>
