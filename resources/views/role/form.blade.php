@extends('layout')

@section('content')
<div class="page-title">
    <h3 class="mb-0">Role</h3>
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="#">Role</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ $is_edit ? 'Edit Data' : 'Add New' }}</li>
    </ol>
</div>

<div class="page-content">
    <form method="POST" action="{{ $action_url }}">
        @csrf
        <div class="row row-wrapper">
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="mb-3">
                            <label for="name" class="form-label">Role Name</label>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Enter text here ..." value="{{ $is_edit ? $data->name : ''}}">
                        </div>
                        <div class="mb-3">
                            <h4 class="mt-0 header-title">Select Permission</h4>
                            <p class="text-muted mb-3">
                                You can select <code class="highlighter-rouge"> more than one</code> permission for this role.
                            </p>
                            <div class="row p-3">
                                <div class="col-12">
                                    <div class="mb-3">
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input check-all" id="checkall"/>
                                            <label class="form-check-label" for="checkall">Select All Permissions</label>
                                        </div>
                                    </div>
                                </div>
                                @foreach($permissions as $permission)
                                <div class="col-2">
                                    <div class="form-group form-check mb-3">
                                        <input type="checkbox" name="permissions[]" value="{{ $permission->id }}" class="form-check-input check-row" id="{{ 'permissions_' . $permission->id }}"
                                        @isset($permissions_role)
                                            @foreach($permissions_role as $item)
                                                @if (isset($data) && $permission->id == $item->permission_id)
                                                    checked
                                                @endif
                                            @endforeach
                                        @endisset>
                                        <label class="form-check-label" for="{{ 'permissions_' . $permission->id }}">{{ $permission->name }}</label>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="row d-flex justify-content-end">
                            <div class="col-auto">
                                <a class="btn btn-light me-2" href="{{ route('role.index') }}">Cancel</a>
                                <button class="btn btn-primary" type="submit">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('script')
<script>
    const lenChecked = $('.check-row:checked').length
    const lenOpt     = $('.check-row').length
    if (lenChecked >= 1) {
        $('#checkall').prop('checked', true)
        if (lenChecked < lenOpt) {
            $('#checkall').prop('indeterminate', true)
        }
    }
</script>
@endsection

