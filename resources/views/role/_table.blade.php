<div class="table-responsive">
    <table class="table align-middle">
        <thead>
            <tr>
                <th>Role Name</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @forelse ($data as $item)
                <tr>
                    <td>{{$item->name}}</td>
                    <td class="table-action">
                        <div class="btn-group dropstart">
                            <a class="btn btn-sm dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                              <i class="fi fi-rr-menu-dots-vertical"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="{{ route("role.edit", ["id"=>$item->id]) }}">Edit</a></li>
                                <li><a class="dropdown-item text-danger" href="#">Delete</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
            @empty
            @include('_partial.table-nodata', ['colspan' => 5])
            @endforelse
        </tbody>
    </table>
</div>
<div class="table-footer">
    <div class="row">
        <div class="col-auto">
            <span class="table-count">Total Role: {{$data->total()}}</span>
        </div>
        <div class="col-auto">
            <div class="d-flex flex-column flex-md-row">
                {{$data->links()}}
                @include('_partial.table-entries')
            </div>
        </div>
    </div>
</div>
