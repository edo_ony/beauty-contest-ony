@extends('layout')

@section('content')
<div class="page-title">
    <div class="row d-flex justify-content-between align-items-center">
        <div class="col">
            <h3 class="mb-0">Role Management</h3>
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="#">Role</a></li>
                <li class="breadcrumb-item active" aria-current="page">List</li>
            </ol>
        </div>
        <div class="col-auto">
            <a class="btn btn-primary" href="{{ route('role.create') }}">Add new</a>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="card">
        <div class="card-body">
            <div class="table-header">
                <div class="row">
                    <div class="col-12 col-md-auto bulk-action">
                        <div class="row g-2">
                            <div class="col-auto">
                                <select class="form-select form-select-sm select2-infinity" aria-label="Bulk action" disabled data-placeholder="Bulk action" data-width="150">
                                    <option value=""></option>
                                    <option value="delete" data-action="#">Delete</option>
                                </select>
                            </div>
                            <div class="col-auto">
                                <button type="button" class="btn btn-primary btn-sm --btn-bulk" disabled>Submit <span class="checked-count badge text-bg-secondary">0</span></button>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <form action="{{route('role.index')}}" id="form-filter">
                                @csrf
                                <div class="row g-2 align-items-center">
                                    <div class="col-12 col-md-auto">
                                      <input type="text" name="search" id="search-data" class="form-control form-control-sm" placeholder="Search data here ...">
                                    </div>
                                    <div class="col-auto">
                                        <button type="submit" class="btn btn-primary btn-sm">Apply</button>
                                    </div>
                                  </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-auto">
                        @include('_partial.table-entries')
                    </div>
                </div>
            </div>
            <div id="dataTable">
                @include('role._table')
            </div>
        </div>
    </div>
</div>
@endsection

