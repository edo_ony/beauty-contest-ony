@extends('layout')

@section('content')


<div class="page-content">
    <div class="row row-wrapper">
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Roda Dua (R2)</h4>
                    <div id="listr2">
                        @include('home._table-parking', ['data'=>$listR2, 'total' => $totalR2])
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Roda Empat (R4)</h4>
                    <div id="listr4">
                        @include('home._table-parking', ['data'=>$listR4, 'total' => $totalR4])
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
