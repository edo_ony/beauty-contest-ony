<div class="table-responsive">
    <table class="table align-middle">
        <thead>
            <tr>
                <th>Nomor Plat</th>
                <th>Waktu Masuk</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($data as $item)
                <tr>
                    <td>{{ $item->vehicle->license_number }}</td>
                    <td>{{ $item->checkin_at->format('d M Y H:i:s') }}</td>
                </tr>
            @empty
                @include('_partial.table-nodata', ['colspan' => 2])
            @endforelse
        </tbody>
    </table>
</div>
<div class="table-footer">
    <div class="row">
        <div class="col-auto">
            <span class="table-count">Total Terparkir: {{$total}}</span>
        </div>
        <div class="col-auto">
            <div class="d-flex flex-column flex-md-row">
                {{$data->links()}}
            </div>
        </div>
    </div>
</div>
