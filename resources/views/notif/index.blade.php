@extends('layout')

@section('content')
<div class="page-title">
    <div class="row d-flex justify-content-between align-items-center">
        <div class="col">
            <h3 class="mb-0">Notification</h3>
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="#">Notification</a></li>
                <li class="breadcrumb-item active" aria-current="page">List</li>
            </ol>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-3 d-flex justify-content-between align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Notification @if($data->count() > 0)<span class="badge bg-primary">{{ $data->count() }}</span>@endif</h5>
                        </div>
                    </div>
                    <div>
                        <div class="list-group mb-3">
                            @forelse ($data as $item)
                                <a href="#" class="list-group-item">
                                    <strong class="d-block">{{ $item->data['title'] }}</strong>
                                    <p class="mb-0">{{ $item->data['desc'] }}</p>
                                    <small>{{ \Carbon\Carbon::parse($item->created_at)->diffForHumans() }}</small>
                                </a>
                            @empty
                                Your Notification is Empty
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

