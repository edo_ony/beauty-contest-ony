@extends('auth.layout')

@section('content')
<div class="card card-login">
    <div class="card-header d-flex justify-content-center">
    </div>
    <div class="card-body has-loading">
        <h3 class="text-center">Sign in</h3>
        <form action="{{route('login.store')}}" method="post">
            @csrf
            <input type="hidden" name="ip" id="userIp">
            <div class="mb-3">
                <label for="username" class="form-label">Username</label>
                <input type="text" name="username" class="form-control" placeholder="Username or email address" id="username">
            </div>
            <div class="mb-3">
                <div class="d-flex justify-content-between">
                    <label for="password" class="form-label">Password</label>
                </div>
                <div class="input-group">
                    <input type="password" name="password" class="form-control --input-pass" id="password" placeholder="Type your password">
                    <span class="input-group-text"><a href="#" class="--show-pass"><i class="fi fi-rr-eye"></i></a></span>
                </div>
            </div>
            <div class="mb-3">
                <button class="btn btn-primary w-100 mb-1" type="submit">Login</button>
            </div>
        </form>
        <div class="text-center mb-2">Or.</div>
        <a href="{{ route('guest.index') }}" class="btn btn-outline-warning w-100 mb-1" type="submit">Login as Guest</a>
    </div>
</div>
@endsection

@section('script')
<script>
    $.get("https://api.ipify.org?format=json", function(response) {
        $('#userIp').val(response.ip);
    }, "json")
</script>
@endsection
