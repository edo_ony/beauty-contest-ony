<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OnPark</title>
    <link rel="icon" type="image/x-icon" href="/assets/img/dummy-logo.png">
    <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/uicons/css/uicons.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('assets/sweetalert/sweetalert.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/daterangepicker/daterangepicker.css') }}" type="text/css" />
    @yield('style-package')
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/parking.css')}}">
    @yield('style')
</head>
<body class="parking">
    @yield('content')

    @yield('components')
    <script src="{{asset('assets/js/jquery-3.6.3.min.js')}}"></script>
    <script src="{{asset('assets/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/momentjs/moment.js')}}"></script>
    <script src="{{asset('assets/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/sweetalert/sweetalert.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="{{asset('assets/js/app.js')}}"></script>
    @yield('script')
</body>
</html>
