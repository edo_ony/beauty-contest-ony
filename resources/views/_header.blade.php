<div class="header">
    <nav class="navbar">
        <div class="container-fluid">
            <div class="d-inline-flex align-items-center">
                <button class="navbar-toggler me-3" type="button">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <button class="navbar-toggler-lg me-3" type="button">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>

            <div>
                <ul class="nav justify-content-center align-items-center">
                    <li class="nav-item">
                      <a class="nav-link has-count" href="{{route('notif.index')}}">
                        <i class="fi fi-rr-bell"></i>@if($unread_notification_count > 0)<span class="notif-count">{{ $unread_notification_count }}</span>@endif
                      </a>
                    </li>
                    <li class="nav-item dropdown dropdown-profile">
                      <a class="nav-link dropdown-toggle caret" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false" href="#">
                        <div class="avatar">
                            <img class="rounded-circle " src="{{asset('assets/img/avatar.webp') }}" alt="">
                        </div>
                        <div class="header-profile d-none d-md-block">
                            <span class="d-block">{{auth()->user()->name}}</span>
                            <small class="d-block">{{auth()->user()->role->name}}</small>
                        </div>
                      </a>
                      <ul class="dropdown-menu dropdown-menu-end">
                        <li><a class="dropdown-item text-muted" href="{{route('logout')}}">Logout</a></li>
                      </ul>
                    </li>
                </ul>
            </div>

        </div>
    </nav>
</div>
