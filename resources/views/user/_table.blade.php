<div class="table-responsive">
    <table class="table align-middle">
        <thead>
        <tr>
        <th class="check">
            <input class="form-check-input check-all" type="checkbox">
        </th>
        <th>Staff Name</th>
        <th>Email</th>
        <th>Status</th>
        <th>Location</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
        @forelse ($data as $item)
            <tr>
                <td class="check"><input class="form-check-input check-row" name="ids[]" type="checkbox" value="{{$item->id}}"></td>
                <td>{{$item->name}}</td>
                <td>{{$item->email}}</td>
                <td>
                    @if ($item->is_active==1)
                    <span class="badge text-bg-success">Active</span>
                    @else
                    <span class="badge text-bg-danger">Inactive</span>
                    @endif
                </td>
                <td>
                    @if ($item->lat && $item->long)
                    <a href="{{ $item->maps_url }}" target="_blank">Check Location</a>
                    @else
                    -
                    @endif
                </td>
                <td class="table-action">
                    <div class="btn-group dropstart">
                        <a class="btn btn-sm dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                          <i class="fi fi-rr-menu-dots-vertical"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a class="dropdown-item" href="{{ route("user.edit", ["id"=>$item->id]) }}">Edit</a></li>
                            <li><a class="dropdown-item text-danger" href="#">Delete</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        @empty
        @include('_partial.table-nodata', ['colspan' => 5])
        @endforelse
    </tbody>
</table>
</div>
<div class="table-footer">
    <div class="row">
        <div class="col-auto">
            <span class="table-count">Total Staff: {{$data->total()}}</span>
        </div>
        <div class="col-auto">
            <div class="d-flex flex-column flex-md-row">
                {{$data->links()}}
                @include('_partial.table-entries')
            </div>
        </div>
    </div>
</div>
