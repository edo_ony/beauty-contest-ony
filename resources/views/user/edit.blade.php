@extends('layout')

@section('content')
<div class="page-title">
    <h3 class="mb-0">User</h3>
    <ol class="breadcrumb mb-0">
        <li class="breadcrumb-item"><a href="#">User</a></li>
        <li class="breadcrumb-item active" aria-current="page">Add New</li>
    </ol>
</div>

<div class="page-content">
    <form method="POST" action="{{ route('user.update', ['id'=>$data->id]) }}">
        @csrf
        <div class="row row-wrapper">
            <div class="col-md-8">
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="mb-3">
                            <label for="role" class="form-label">Role</label>
                            <select class="form-select select2" name="role" data-placeholder="Select here" data-allow-clear="false">
                                <option value=""></option>
                                @foreach ($roles as $item)
                                @if ($item->id == $data->user_role_id)
                                <option value="{{$item->id}}" selected>{{$item->name}}</option>
                                @else
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="name" class="form-label">User Name</label>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Enter text here ..." value="{{$data->name}}">
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" name="email" id="email" class="form-control" placeholder="Enter text here ..." value="{{$data->email}}">
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Password</label>
                            <input type="password" name="password" id="password" class="form-control" placeholder="Enter text here ...">
                        </div>
                        <div class="mb-3">
                            <label for="password_confirmation" class="form-label">Re-type Password</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Enter text here ...">
                        </div>
                        <div class="mb-3">
                            <label for="status" class="form-label">Status</label>
                            <div>
                                <div class="toggle-btn">
                                    <input class="form-check-input" type="checkbox" role="switch" id="status" name="status" value="1" {{ $data->is_active?'checked':'' }}>
                                    <label for="status" data-off="Inactive" data-on="Active"></label>
                                </div>
                            </div>
                        </div>

                        <div class="row d-flex justify-content-end">
                            <div class="col-auto">
                                <a class="btn btn-light me-2" href="{{ route('user.index') }}">Cancel</a>
                                <button class="btn btn-primary" type="submit">Save</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
