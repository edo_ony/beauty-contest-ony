# Beauty Contest Ony - OnPark

## Requirements

PHP 8.0+

## Installation

Generate .env file by copying from .env.example:
```
cp .env.example .env
```

Use [composer](https://getcomposer.org/) to update the library and its dependencies.
```
composer update
```

Then generate APP_KEY:
```
php artisan key:generate
```

Prepare database table, then do seeding data
```
php artisan migrate:fresh --seed
```

## Usage

```
# Live URL
https://oninyon.com/dot/login

# Role Manage
username : manager
password : password

# Role Staff
username : staff
password : password
```