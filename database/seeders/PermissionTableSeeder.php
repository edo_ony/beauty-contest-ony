<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permissions;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissionData = [
            [ 'name' => 'Dashboard', 'slug' => 'dashboard', 'nested' => '1' ],

            [ 'name' => 'User', 'slug' => 'user', 'nested' => '1' ],

            [ 'name' => 'Role', 'slug' => 'role', 'nested' => '1' ],

            [ 'name' => 'Permission', 'slug' => 'permission', 'nested' => '1' ],

            [ 'name' => 'Parking', 'slug' => 'parking', 'nested' => '1' ],

            [ 'name' => 'Settings', 'slug' => 'settings', 'nested' => '1' ],
            [ 'name' => 'View Settings', 'slug' => 'view.settings', 'nested' => '6' ],
            [ 'name' => 'Save Settings', 'slug' => 'save.settings', 'nested' => '6' ],
        ];
        foreach ($permissionData as $data) {
            Permissions::updateOrCreate(['slug'=>$data['slug']], $data);
        }
    }
}
