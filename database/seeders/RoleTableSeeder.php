<?php
namespace Database\Seeders;

use App\Models\UserRole;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleData = [
            [
                'name' => 'Manager',
                'slug' => 'manager',
            ],
            [
                'name' => 'Staff',
                'slug' => 'staff',
            ],
            [
                'name'  => 'Customer',
                'slug'  => 'customer',
            ]
        ];
        foreach ($roleData as $data) {
            UserRole::updateOrCreate(['slug'=>$data['slug']], $data);
        }
    }
}
