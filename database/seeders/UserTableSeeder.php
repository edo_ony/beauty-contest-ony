<?php
namespace Database\Seeders;

use App\Models\User;
use App\Models\UserRole;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userData = [
                [
                    'name'      => 'Manager',
                    'username'  => 'manager',
                    'email'     => 'manager@mail.com',
                    'password'  => Hash::make('password'),
                    'is_active' => true,
                    'user_role_id' => UserRole::where('slug','manager')->first()->id
                ],
                [
                    'name'      => 'Staff',
                    'username'  => 'staff',
                    'email'     => 'staff@mail.com',
                    'password'  => Hash::make('password'),
                    'is_active' => true,
                    'user_role_id' => UserRole::where('slug','staff')->first()->id
                ],
                [
                    'name'      => 'Customer',
                    'username'  => 'customer',
                    'email'     => 'customer@mail.com',
                    'password'  => Hash::make('password'),
                    'is_active'     => true,
                    'user_role_id'  => UserRole::where('slug','customer')->first()->id
                ],
            ];
        foreach ($userData as $data) {
            User::updateOrCreate(['username'=>$data['username']], $data);
        }
    }
}
