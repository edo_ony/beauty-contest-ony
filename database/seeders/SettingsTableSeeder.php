<?php
namespace Database\Seeders;

use App\Models\Settings;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleData = [
            [
                'name' => 'capacity_r2',
                'value' => '50',
            ],
            [
                'name' => 'rates_r2',
                'value' => '1000',
            ],
            [
                'name' => 'capacity_r4',
                'value' => '30',
            ],
            [
                'name' => 'rates_r4',
                'value' => '2000',
            ],
        ];

        foreach ($roleData as $data) {
            Settings::updateOrCreate(['name'=>$data['name']], $data);
        }
    }
}
