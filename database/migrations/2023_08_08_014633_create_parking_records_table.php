<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParkingRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parking_records', function (Blueprint $table) {
            $table->id();
            $table->foreignId('vehicle_id')->constrained();
            $table->dateTime('checkin_at');
            $table->unsignedBigInteger('checkin_by');
            $table->foreign('checkin_by')->on('users')->references('id');
            $table->dateTime('checkout_at')->nullable();
            $table->unsignedBigInteger('checkout_by')->nullable();
            $table->foreign('checkout_by')->on('users')->references('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parking_records');
    }
}
