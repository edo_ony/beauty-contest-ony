<?php
namespace App\Services;

use App\Repositories\ParkingRecordRepository;
use App\Services\Utils\SlugTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ParkingRecordService extends BaseService
{
    use SlugTrait;

    protected function makeRepo()
    {
        return new ParkingRecordRepository;
    }

    public function checkIn(Request $req)
    {
        $parked = $this->getRepo()->getParked($req->nomor_plat);
        if ($parked) {
            return false;
        }

        $date = now();
        $user = auth()->user();

        $vehicleService = new VehicleService();

        $vehicle = $vehicleService->getByLicense($req->nomor_plat, $req->type);

        if (!$vehicle) {
            $vehicle = $vehicleService->insert($req);
        }

        $settingService = new SettingService();
        $rates          =  $settingService->find('rates_' . $req->type);

        $data = [
            'vehicle_id'        => $vehicle->id,
            'checkin_at'        => $date,
            'checkin_by'        => $user->id,
            'rates'             => $rates->value
        ];

        return $this->getRepo()->create($data);
    }

    public function checkOut(Request $req)
    {
        $user = auth()->user();
        $date = now();

        $record = $this->getRepo()->getParked($req->nomor_plat);
        if ($record) {
            $data = [
                'checkout_at'   => $date,
                'checkout_by'   => $user->id,
            ];

            $record = $this->getRepo()->update($record->id, $data);

            $rates      = $record->rates;
            $in         = Carbon::parse($record->checkin_at);
            $out        = Carbon::parse($record->checkout_at);
            $diff       = $in->floatDiffInHours($out);
            $duration   = ceil($diff*1);

            $total = $rates*$duration;

            return [$rates, $total, $duration, $in->format('d-m-Y H:i:s'), $out->format('d-m-Y H:i:s')];

        }

        return false;
    }

    public function countParked($type)
    {
        return $this->getRepo()->countParked($type);
    }

    public function getTopTen($type)
    {
        return $this->getRepo()->getTopTen($type);
    }

    public function getParkedByType($type)
    {
        return $this->getRepo()->getParkedList($type);
    }

    /**
     * Get parking settings
     *
     * @return array
     */
    public function getSettings()
    {
        $settingService = new SettingService();
        $ratesR2  =  $settingService->find('rates_r2');
        $ratesR4  =  $settingService->find('rates_r4');
        $capacityR2  =  $settingService->find('capacity_r2');
        $capacityR4  =  $settingService->find('capacity_r4');

        return [
            'capacityR2' => $capacityR2->value,
            'capacityR4' => $capacityR4->value,
        ];
    }


}
