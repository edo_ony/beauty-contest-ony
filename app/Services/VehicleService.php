<?php
namespace App\Services;

use App\Repositories\VehicleRepository;
use App\Services\Utils\SlugTrait;
use Illuminate\Http\Request;

class VehicleService extends BaseService
{
    use SlugTrait;

    protected function makeRepo()
    {
        return new VehicleRepository;
    }

    public function getByLicense($licenseNumber, $type)
    {
        return $this->getRepo()->getOne([
            'license_number'    => ['=', $licenseNumber],
            'type'              => ['=', $type]
        ]);
    }

    public function insert($req)
    {
        $user = auth()->user();
        
        $data = [
            'user_id'        => $user->id,
            'license_number' => $req->nomor_plat,
            'type'           => $req->type,
        ];

        return $this->getRepo()->create($data);
    }

}
