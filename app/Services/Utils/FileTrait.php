<?php
namespace App\Services\Utils;

use Carbon\Carbon;
use Illuminate\Support\Str;

trait FileTrait
{
    protected function fileName($file = null)
    {
        $fname = Carbon::now()->toDateString().'-'.uniqid();

        $extension = $file ? '.'.$file->extension() : '';

        return $fname.$extension;
    }
}
