<?php
namespace App\Services\Utils;

use Illuminate\Support\Facades\Password;

trait ResetPassword
{
    public function passwordReset($user)
    {
        $password = Password::createToken($user);

        return $password;
    }

    public function validateToken($user, $token)
    {
        return Password::tokenExists($user, $token);
    }

    public function clearToken($user)
    {
        return Password::deleteToken($user);
    }
}
