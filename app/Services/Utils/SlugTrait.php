<?php
namespace App\Services\Utils;

use Illuminate\Support\Str;

trait SlugTrait
{
    protected $exceptionID = null;

    protected function except(int $id)
    {
        $this->exceptionID = $id;

        return $this;
    }

    protected function generateSlug($title, $field = 'slug')
    {
        $slug = Str::slug($title);
        $makeSlug = $slug;
        do {
            $params = [$field => ['=', $makeSlug]];
            if ($this->exceptionID) {
                $params['id'] = ['!=', $this->exceptionID];
            }
            $find = $this->getRepo()->getOne($params);
            if ($find) {
                $makeSlug = $slug.'-'.strtolower(Str::random(7));
            }
        } while ($find);

        return $makeSlug;
    }
}
