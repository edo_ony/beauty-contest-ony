<?php
namespace App\Services;

use App\Repositories\PermissionRoleRepository;
use App\Services\Utils\SlugTrait;
use Illuminate\Http\Request;

class PermissionRoleService extends BaseService
{
    use SlugTrait;

    protected function makeRepo()
    {
        return new PermissionRoleRepository;
    }

    public function getPermissionsByRoleId($roleId)
    {
        return $this->getRepo()->getPermissionsByRoleId($roleId);
    }
}
