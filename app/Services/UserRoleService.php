<?php
namespace App\Services;

use App\Repositories\UserRoleRepository;
use App\Services\Utils\SlugTrait;
use Illuminate\Http\Request;

class UserRoleService extends BaseService
{
    use SlugTrait;

    /**
     * Undocumented function
     *
     * @return Repository
     */
    protected function makeRepo()
    {
        return new UserRoleRepository;
    }

    public function insert(Request $req)
    {
        $data = [
            'name' => $req->input('name'),
            'slug' => $this->generateSlug($req->name),
        ];

        return $this->getRepo()->create($data);
    }

    public function update(Request $req)
    {
        $data = [
            'name' => $req->input('name'),
            'slug' => $this->except($req->id)->generateSlug($req->name),
        ];
        $permissions = $req->permissions;

        return $this->getRepo()->updateAndSync($req->id, $data, $permissions);
    }

}
