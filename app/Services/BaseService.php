<?php
namespace App\Services;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

abstract class BaseService
{
    private $repo;

    function __construct()
    {
        $this->setRepo($this->makeRepo());
    }

    protected abstract function makeRepo();

    private function setRepo($repo)
    {
        $this->repo = $repo;

        return $this;
    }

    protected function getRepo()
    {
        return $this->repo;
    }

    public function getById($id)
    {
        return $this->repo->getOne(['id' => [ '=', $id]]);
    }

    public function getBySlug($slug, $fieldname = 'slug')
    {
        return $this->repo->getOne([$fieldname => ['=', $slug]]);
    }

    public function getPaginate(Request $req)
    {
        $params = $req->except(['order','sort','page', '_token']);

        return $this->repo->setPerpage($req->get('perpage', 15))->getPaginate($params, $req->get('order'), $req->get('sort'));
    }

    public function getAll(Request $req)
    {
        $params = $req->except(['order','sort']);

        return $this->repo->getAll($params, $req->get('order',''), $req->get('sort',''));
    }

    public function getDropdown($order = 'name', $params = [])
    {
        return $this->repo->getAll($params, $order,'asc');
    }

    public function delete($ids)
    {
        return $this->repo->delete($ids);
    }

    public function deleteHasImage($ids, $image = 'image')
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        foreach ($ids as $id) {
            $data = $this->getById($id);
            if ($data) {
                $imgPath = $data->{$image};
                if (!empty($imgPath)) {
                    Storage::delete($imgPath);
                }
            }
            $this->repo->delete($ids);
        }

        return true;
    }
}
