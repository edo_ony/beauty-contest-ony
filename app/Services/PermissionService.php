<?php
namespace App\Services;

use App\Repositories\PermissionRepository;
use App\Services\Utils\SlugTrait;
use Illuminate\Http\Request;

class PermissionService extends BaseService
{
    use SlugTrait;

    protected function makeRepo()
    {
        return new PermissionRepository;
    }

    public function insert(Request $req)
    {
        $data = [
            'name'          => $req->input('name'),
            'slug'          => $this->generateSlug($req->name),
            'description'   => $req->input('description'),
        ];

        return $this->getRepo()->create($data);
    }

    public function update(Request $req)
    {
        $data = [
            'name' => $req->input('name'),
            'slug' => $this->except($req->id)->generateSlug($req->name),
        ];

        return $this->getRepo()->update($req->id, $data);
    }
}
