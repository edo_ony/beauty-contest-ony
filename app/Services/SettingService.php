<?php
namespace App\Services;

use App\Repositories\SettingRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SettingService extends BaseService
{

    protected function makeRepo()
    {
        return new SettingRepository;
    }

    public function update(Request $req)
    {
        $updated = null;
        foreach ($req->settings as $name => $value) {
            $updated = $this->getRepo()->updateByName($name, $value);
        }

        return $updated;
    }
    
    public function find(string $name)
    {
        $data = $this->getRepo()->getOne(['name'=>['=', $name]]);

        return $data;
    }

    public function getByPrefix($prefix)
    {
        return $this->getRepo()->getAllByPrefix($prefix);
    }

    public function getAllSettings()
    {
        $settings = $this->getAll(new Request);

        $data = [];

        foreach ($settings as $value) {
            $data[$value->name] = $value->value;
        }

        return (object) $data;

    }

}
