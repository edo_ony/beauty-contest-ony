<?php
namespace App\Services;

use App\Repositories\UserRepository;
use App\Services\Utils\ResetPassword;
use App\Services\Utils\SlugTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;
use App\Notifications\UserRoleChange;
use App\Notifications\UserDataChange;
use App\Notifications\UserPasswordChange;

class UserService extends BaseService
{
    use SlugTrait, ResetPassword;

    /**
     * Undocumented function
     *
     * @return Repository
     */
    protected function makeRepo()
    {
        return new UserRepository;
    }

    /**
     * Login regular
     *
     * @param Request $req
     * @return void
     */
    public function login(Request $req)
    {
        $attempt = Auth::attempt(['email' => $req->username, 'password' => $req->password]);
        if (!$attempt) {
            Auth::attempt(['username' => $req->username, 'password' => $req->password]);

            $resp = Http::get('http://ip-api.com/json/'.$req->ip)->json();
            $data = [
                'lat'  => $resp['lat'],
                'long' => $resp['lon']
            ];

            return $this->getRepo()->update(Auth::user()->id, $data);
        }

        return Auth::user();
    }


    public function insert(Request $req)
    {
        $data = [
            'name'          => $req->input('name'),
            'username'      => $this->generateSlug($req->name, 'username'),
            'email'         => $req->input('email'),
            'password'      => Hash::make($req->input('password')),
            'is_active'     => $req->input('status', '0'),
            'user_role_id'  => $req->input('role'),
        ];

        if ($req->hasFile('photo')) {
            $path = $req->file('photo')->store('images');

            $data['photo'] = $path;
        }

        return $this->getRepo()->create($data);
    }

    public function update(Request $req)
    {
        $user = $this->getById($req->id);

        $data = [
            'name'          => $req->input('name'),
            'username'      => $this->except($req->id)->generateSlug($req->name, 'username'),
            'email'         => $req->input('email'),
            'is_active'     => $req->input('status', '0'),
            'user_role_id'  => $req->input('role'),
        ];

        if ($req->filled('password')) {
            $data['password'] = Hash::make($req->input('password'));
            $user->notify(new UserPasswordChange());
        }

        if ($req->hasFile('photo')) {
            if($user->photo){
                Storage::delete($user->photo);
            }

            $path = $req->file('photo')->store('images');

            $data['photo'] = $path;
        }

        if ($user->user_role_id != $req->input('role')) {
            $user->notify(new UserRoleChange());
        }

        if ($user->user_role_id == $req->input('role') && !$req->filled('password')) {
            $user->notify(new UserDataChange());
        }

        return $this->getRepo()->update($req->id, $data);
    }


}
