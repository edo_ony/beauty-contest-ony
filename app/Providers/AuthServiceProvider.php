<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->internalPolicies();
    }

    public function internalPolicies()
    {
        Gate::define('dashboard', function($s) { return $s->hasAccess('dashboard'); });
        Gate::define('user', function($s) { return $s->hasAccess('user'); });
        Gate::define('role', function($s) { return $s->hasAccess('role'); });
        Gate::define('permission', function($s) { return $s->hasAccess('permission'); });
        Gate::define('parking', function($s) { return $s->hasAccess('parking'); });
        Gate::define('settings', function($s) { return $s->hasAccess('settings'); });

        Gate::define('view.settings', function($s) { return $s->hasAccess('view.settings'); });
        Gate::define('save.settings', function($s) { return $s->hasAccess('save.settings'); });
    }
}
