<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

        // Using view composer to set following variables globally
        view()->composer('*',function($view) {
            if (auth()->check()) {
                $view->with('unread_notification_count', Auth::user()->unreadNotifications->count());
            }
        });
    }
}
