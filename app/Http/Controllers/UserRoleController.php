<?php
namespace App\Http\Controllers;


use App\Services\UserRoleService;
use App\Services\PermissionService;
use App\Services\PermissionRoleService;
use Exception;
use Illuminate\Http\Request;

class UserRoleController extends Controller
{
    protected $service;

    protected $data = [];

    function __construct(UserRoleService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $this->pageComponents($request);

        $this->data['data'] = $this->service->getPaginate($request);

        if ($request->ajax()) {
            return response()->json([
                'html' => view('role._table', $this->data)->render()
            ]);
        }

        return view('role.index', $this->data);
    }

    public function create(PermissionService $permissionService)
    {
        $this->data['action_url']  = route('role.store');
        $this->data['is_edit']     = false;
        $this->data['permissions'] = $permissionService->getAll(new Request);

        return view('role.form', $this->data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required'
        ]);

        try {
            $this->service->insert($request);

            return response()->json(['redirect'=>route('role.index')]);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th->getMessage()]);
        }
    }

    public function edit(Request $request, PermissionService $permissionService, PermissionRoleService $permissionRoleService)
    {
        try {
            $this->data['data']             = $this->service->getById($request->id);
            $this->data['action_url']       = route('role.update', ['id'=>$request->id]);
            $this->data['is_edit']          = true;
            $this->data['permissions']      = $permissionService->getAll(new Request);
            $this->data['permissions_role'] = $permissionRoleService->getPermissionsByRoleId($request->id);

            if(!$this->data['data']) {
                throw new Exception('No data');
            }

            return view('role.form', $this->data);
        } catch (\Throwable $th) {
            return redirect()->route('role.index');
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'name'      => 'required'
        ];
        $request->validate($rules);

        try {
            $this->service->update($request);

            return response()->json(['redirect'=>route('role.index')]);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th->getMessage()]);
        }
    }

}
