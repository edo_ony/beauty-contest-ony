<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $service;

    protected $data = [];

    protected function pageComponents(Request $request)
    {
        $this->data['entries'] = [15, 50, 100];
        $this->data['perpage'] = $request->query('perpage', 15);
    }

    public function delete(Request $request)
    {
        try {
            $deleted = $this->service->delete($request->id);
            if ($deleted) {
                return response()->json(['message'=>trans('msg.delete.success'),'success'=>true, 'reload'=>true]);
            }
        } catch (\Throwable $th) {
            return response()->json(['message'=>trans('msg.delete.failed'),'success'=>false]);
        }
    }

    public function updateStatus(Request $request)
    {
        try {
            $updated = $this->service->updateStatus($request);

            return response()->json(['message'=>[
                'type'  => 'success',
                'title' => 'Success',
                'desc'  => 'Status changed successfully',
            ],'success'=>true, 'reload'=>1]);
        } catch (\Throwable $th) {
            return response()->json(['message'=>[
                'type'  => 'error',
                'title' => 'Failed',
                'desc'  => 'Failed to change status changed',
            ],'success'=>false]);
        }
    }

    public function bulkAction(Request $request)
    {
        if ($request->type == 'delete') {
            $deleted = $this->service->delete($request->ids);
            if ($deleted) {
                return response()->json(['message' =>[
                    'type'  => 'success',
                    'title' => 'Success',
                    'desc'  => count($request->ids).' data(s) deleted successfully',
                ], 'success'=>true, 'reload'=>true]);
            }
        }
    }
}
