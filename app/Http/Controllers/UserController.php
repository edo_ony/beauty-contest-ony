<?php
namespace App\Http\Controllers;


use App\Services\UserRoleService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class UserController extends Controller
{
    protected $service;

    function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $this->pageComponents($request);

        $this->data['data'] = $this->service->getPaginate($request);

        if ($request->ajax()) {
            return response()->json([
                'html' => view('user._table', $this->data)->render()
            ]);
        }

        return view('user.index', $this->data);
    }

    public function create(UserRoleService $userRoleService)
    {
        $this->data['roles'] = $userRoleService->getDropdown();

        return view('user.create', $this->data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'role'      => 'required',
            'name'      => 'required',
            'email'     => 'required|email|unique:users,email',
            'password'  => 'required|confirmed|min:6',
        ]);

        try {
            $this->service->insert($request);

            return response()->json(['redirect'=>route('user.index')]);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th->getMessage()]);
        }
    }

    public function edit(Request $request, UserRoleService $userRoleService)
    {
        try {
            $this->data['data'] = $this->service->getById($request->id);

            if(!$this->data['data']) {
                throw new Exception('No data');
            }

            $this->data['roles'] = $userRoleService->getDropdown();

            return view('user.edit', $this->data);
        } catch (\Throwable $th) {
            return redirect()->route('user.index');
        }
    }

    public function update(Request $request)
    {
        $rules = [
            'role'      => 'required',
            'name'      => 'required',
            'email'     => [
                'required','email',
                Rule::unique('users')->ignore($request->id)
            ]
        ];
        if ($request->password) {
            $rules['password'] = 'confirmed|min:6';
        }
        $request->validate($rules);

        try {
            $this->service->update($request);

            return response()->json(['redirect'=>route('user.index')]);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th->getMessage()]);
        }
    }

}
