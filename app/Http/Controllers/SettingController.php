<?php
namespace App\Http\Controllers;


use App\Services\SettingService;
use Exception;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    protected $service;

    protected $data = [];

    function __construct(SettingService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $this->data['data'] = $this->service->getAllSettings();

        return view('settings.index', $this->data);
    }

    public function update(Request $request)
    {
        try {
            $this->service->update($request);

            return response()->json([
                'message' => [
                    'title'  => 'Setting berhasi diperbaharui',
                    'type'  => 'success'
                ],
                'redirect'=>route('settings.index')]);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th->getMessage()]);
        }
    }

}
