<?php
namespace App\Http\Controllers;


use App\Services\ParkingRecordService;
use App\Services\SettingService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ParkingRecordController extends Controller
{
    protected $service;

    protected $data = [];

    function __construct(ParkingRecordService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return view('parking.index', $this->data);
    }

    public function indexMode(Request $request)
    {
        $this->data['mode']         = $request->mode;
        $this->data['modeLabel']    = $request->mode == 'checkout' ? 'keluar' : 'masuk';

        return view('parking.mode', $this->data);
    }

    public function indexType(Request $request, SettingService $settingService)
    {
        $this->data['capacity']     = $settingService->find('capacity_' . $request->type);
        $this->data['countParked']  = $this->service->countParked($request->type);

        $this->data['mode']         = $request->mode;
        $this->data['type']         = $request->type;
        $this->data['modeLabel']    = $request->mode == 'checkout' ? 'keluar' : 'masuk';

        return view('parking.type', $this->data);
    }

    public function storeCheckin(Request $request, SettingService $settingService)
    {
        $request->validate([
            'nomor_plat'      => 'required|regex:/^[A-Z]{1,2}\s{1}\d{1,4}\s{1}[A-Z]{1,3}$/',
        ], [
            'nomor_plat.required' => 'Nomor plat kendaraan wajib diisi.',
            'nomor_plat.regex' => 'Format nomor plat kendaraan tidak benar.',
        ]);

        try {
            $parked     = $this->service->countParked($request->type);
            $capacity   = $settingService->find('capacity_' . $request->type);

            if ($parked == $capacity->value) {
                return response()->json([
                    'message' => [
                        'title' => 'Maaf parkir sudah penuh!',
                        'type'  => 'error'
                    ],
                    'reset' => true
                ]);
            }

            $available = $this->service->checkIn($request);
            if (!$available) {
                throw new Exception("$request->nomor_plat masih terdaftar parkir!", 1);
            }

            $parked     = $this->service->countParked($request->type);

            if ($parked == $capacity->value) {
                return response()->json([
                    'message' => [
                        'title' => $request->nomor_plat,
                        'html'  => 'Berhasil Parkir Masuk<br/><strong class="text-danger">Parkir sudah penuh!</strong>',
                        'type'  => 'warning'
                    ],
                    'reset' => true
                ]);
            }

            Cache::increment('slot_filled_'.$request->type);

            $filled = Cache::get('slot_filled_'.$request->type);

            $avail = ($capacity->value - $filled);
            Cache::forget('slot_available_'.$request->type);
            Cache::put('slot_available_'.$request->type, $avail);

            return response()->json([
                'alert' => [
                    'title' => $request->nomor_plat,
                    'desc'  => 'Berhasil Parkir Masuk',
                    'type'  => 'success'
                ],
                'dom' => [
                    'id'        => 'slotCounter',
                    'content'   => $filled
                ],
                'reset' => true
            ]);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th->getMessage()]);
        }
    }

    public function storeCheckout(Request $request)
    {
        $request->validate([
            'nomor_plat'      => 'required',
        ]);

        try {
            $find = $this->service->checkOut($request);

            if(!$find) {
                throw new Exception("$request->nomor_plat tidak terdaftar parkir!", 1);
            }

            list($rates, $total, $duration, $in, $out) = $find;

            $filled = Cache::decrement('slot_filled_'.$request->type);
            $avail = Cache::get('slot_available_'.$request->type);

            Cache::forget('slot_available_'.$request->type);
            Cache::put('slot_available_'.$request->type, ($avail + 1));

            return response()->json([
                    'message' => [
                        'title' => 'Rp ' . ($total),
                        'html'  => '<strong>'.$request->nomor_plat.'</strong>' . '<br/>Masuk: ' . $in .'<br/>Keluar: ' . $out.'<br/>Durasi Terhitung: ' . $duration .' jam',
                        'type'  => 'success'
                    ],
                    'dom' => [
                        'id'        => 'slotCounter',
                        'content'   => $filled
                    ],
                    'reset' => true
                ]);
        } catch (\Throwable $th) {
            return response()->json(['error' => $th->getMessage()]);
        }
    }

    /**
     * Ajax function
     *
     * @return void
     */
    public function getSlot()
    {
        $data = [
            'slotFilledR2'    => Cache::get('slot_filled_r2', 0),
            'slotFilledR4'    => Cache::get('slot_filled_r4', 0),
            'slotAvailableR2' => Cache::get('slot_available_r2', $this->service->getSettings()['capacityR2']),
            'slotAvailableR4' => Cache::get('slot_available_r4', $this->service->getSettings()['capacityR4']),
        ];

        return response()->json(['data' => $data]);
    }


}
