<?php
namespace App\Http\Controllers;

use App\Services\UserService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $service;

    protected $data = [];

    function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function login()
    {
        return view('auth.login');
    }

    public function storeLogin(Request $request)
    {
        $request->validate([
            'username'  => 'required',
            'password'  => 'required',
        ]);

        try {
            $loggedin = $this->service->login($request);
            if (!$loggedin) {
                throw new Exception('Username or Password salah');
            }

            return response()->json(['redirect'=>route('dashboard')]);
        } catch (\Throwable $th) {
            return response()->json(['errors' => ['password'=>[$th->getMessage()], 'message' => 'The given data was invalid.']], 422);
        }
    }

    public function logout(Request $req)
    {
        $user = Auth::user();
        if ($user->subscribe) {
            $user->deletePushSubscription($user->subscribe->endpoint);
        }

        Auth::logout();

        return redirect()->route('login.index');
    }
}
