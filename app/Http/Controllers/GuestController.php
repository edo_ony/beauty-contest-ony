<?php
namespace App\Http\Controllers;

use App\Services\ParkingRecordService;
use App\Services\SettingService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class GuestController extends Controller
{
    protected $service;

    protected $data = [];

    function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function index(SettingService $settingService)
    {
        $totalR2 = Cache::get('slot_filled_r2');
        $totalR4 = Cache::get('slot_filled_r4');

        $settings = $settingService->getAllSettings();

        $capacityR2 = $settings->capacity_r2;
        $capacityR4 = $settings->capacity_r4;

        $this->data['ratesR2'] = number_format($settings->rates_r2,0,',','.');
        $this->data['availableR2'] = $capacityR2 - $totalR2;

        $this->data['ratesR4'] = number_format($settings->rates_r4,0,',','.');
        $this->data['availableR4'] = $capacityR4 - $totalR4;


        return view('guest.index', $this->data);
    }


}
