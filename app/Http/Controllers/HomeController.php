<?php
namespace App\Http\Controllers;

use App\Services\ParkingRecordService;
use App\Services\UserService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $service;

    protected $data = [];

    function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request, ParkingRecordService $parkingService)
    {
        $this->data['totalR2'] = $parkingService->countParked('r2');
        $this->data['totalR4'] = $parkingService->countParked('r4');

        $this->data['listR2']  = $parkingService->getParkedByType('r2');
        $this->data['listR2']->appends(['type'=>'r2']);
        if ($request->ajax() && $request->type == 'r2') {
            return response()->json([
                'html' => view('home._table-parking', ['data' => $this->data['listR2'], 'total' => $this->data['totalR2']])->render(),
                'section' => 'listr2'
            ]);
        }

        $this->data['listR4']  = $parkingService->getParkedByType('r4');
        $this->data['listR4']->appends(['type'=>'r4']);
        if ($request->ajax() && $request->type == 'r4') {
            return response()->json([
                'html' => view('home._table-parking', ['data' => $this->data['listR4'], 'total' => $this->data['totalR4']])->render(),
                'section' => 'listr4'
            ]);
        }


        return view('home.index', $this->data);
    }


}
