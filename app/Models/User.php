<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use NotificationChannels\WebPush\HasPushSubscriptions;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasPushSubscriptions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo(UserRole::class, 'user_role_id');
    }

    public function hasAccess($access) : bool
    {
        foreach ($this->role->permissions as $list) {
            if ($list->slug == $access) {
                return true;
            }
        }

        return false;
    }

    public function getMapsUrlAttribute($value)
    {
        // https://www.google.com/maps/search/?api=1&query=-7.7857304,110.3666397
        return "https://www.google.com/maps/search/?api=1&query=".$this->lat.",".$this->long;
    }

    public function subscribe()
    {
        return $this->hasOne(PushSubscribe::class, 'subscribable_id');
    }
}
