<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserRole extends Model
{
    use HasFactory, Notifiable;

    protected $guarded = ['id'];

    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permissions','permission_roles','role_id','permission_id');
    }
}
