<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PushSubscribe extends Model
{
    use HasFactory;

    protected $table = 'push_subscriptions';
}
