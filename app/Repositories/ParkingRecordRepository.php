<?php
namespace App\Repositories;

use App\Models\ParkingRecord;

class ParkingRecordRepository extends BaseRepository
{
    protected function makeModel()
    {
        return new ParkingRecord;
    }

    public function getParked($licenseNumber)
    {
        $query = $this->getModel()->select(['parking_records.*']);

        $query->join('vehicles as v', 'v.id', '=', 'parking_records.vehicle_id');
        $query->where('v.license_number', '=', $licenseNumber);
        $query->whereNull('checkout_at');

        return $query->first();
    }

    public function countParked($type)
    {
        $query = $this->getModel()->select(['parking_records.*']);
        $query->join('vehicles as v', 'v.id', '=', 'parking_records.vehicle_id');
        $query->where('v.type', '=', $type);

        $query->whereNull('checkout_at');

        return $query->count();
    }

    public function getTopTen($type)
    {
        $query = $this->getModel()->select(['parking_records.*']);

        $query->join('vehicles as v', 'v.id', '=', 'parking_records.vehicle_id');
        $query->where('v.type', '=', $type);

        $query->orderBy('created_at','desc');
        $query->limit(10);

        return $query->get();
    }

    public function getParkedList($type)
    {
        $query = $this->getModel()->select(['parking_records.*']);

        $query->join('vehicles as v', 'v.id', '=', 'parking_records.vehicle_id');
        $query->where('v.type', '=', $type);
        $query->whereNull('checkout_at');
        $query->orderBy('checkin_at','desc');

        return $query->simplePaginate(10);
    }
}
