<?php
namespace App\Repositories;

use App\Models\UserRole;
use Illuminate\Support\Facades\DB;

class UserRoleRepository extends BaseRepository
{
    protected function makeModel()
    {
        return new UserRole;
    }

    public function getPaginate(array $params, $order, $sort)
    {
        $query = $this->getModel()->select(['user_roles.*']);

        if (!empty($params['search'])) {
            $query->where(DB::raw('lower(name)'), 'like', '%'.strtolower($params['search']).'%');
        }

        return $query->paginate();
    }

    public function updateAndSync(int $id, array $data, $permissions)
    {
        $this->getModel()->where('id', $id)->update($data);

        return $this->getOne(['id' => ['=', $id]])->permissions()->sync($permissions);
    }
}
