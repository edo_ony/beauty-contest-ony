<?php
namespace App\Repositories;

use App\Models\PermissionRoles;

class PermissionRoleRepository extends BaseRepository
{
    protected function makeModel()
    {
        return new PermissionRoles;
    }

    public function getPermissionsByRoleId($roleId)
    {
        return $this->getModel()->where('role_id', $roleId)->get();
    }
}
