<?php
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{
    /**
     * Undocumented variable
     *
     * @var Model
     */
    private $model;

    protected $customer = false;

    protected $perPage = 15;

    function __construct()
    {
        $this->setModel($this->makeModel());
    }

    protected abstract function makeModel();

    protected function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Insert a record into database.
     *
     * @param array $data ['colName'=>'value','colName2'=>'value2',...]
     * @return resource inserted data
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Get one database record with or withour parameters.
     *
     * @param array $params where: ['colName'=>['=','value']] | whereNot: ['colName'=>['!=','value']]
     * @return Model
     */
    public function getOne(array $params)
    {
        $query = $this->model->select('*');
        if ($params) {
            $this->where($query, $params);
        }

        return $query->first();
    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @return Model
     */
    public function getById(int $id)
    {
        return $this->getOne(['id' => [ '=', $id]]);
    }

    public function setPerPage($offset = 15)
    {
        $this->perPage = $offset;

        return $this;
    }

    public function get(array $params, bool $paginate, $order, $sort)
    {
        $query = $this->model->select('*');
        if (count($params)) {
            $this->where($query, $params);
        }

        if (!empty($order) && !empty($sort)) {
            $query->orderBy($order, $sort);
        }

        return $paginate ? $query->paginate($this->perPage) : $query->get();
    }

    /**
     * Undocumented function
     *
     * @param array $params ['field_name' => ['operator', 'value]] (example: ['id'=>['=',1]])
     * @param string $order 'field_name'
     * @param string $sort  asc|desc
     * @return void
     */
    public function getAll(array $params, string $order, string $sort)
    {
        return $this->get($params, false, $order, $sort);
    }

    /**
     * Undocumented function
     *
     * @param array $params ['field_name' => ['operator', 'value]] (example: ['id'=>['=',1]])
     * @param string $order 'field_name'
     * @param string $sort  asc|desc
     * @return void
     */
    public function getPaginate(array $params, $order, $sort)
    {
        return $this->get($params, true, $order, $sort);
    }

    /**
     * Update a record by data ID
     *
     * @param integer $id
     * @param array $data ['colName'=>'value','colName2'=>'value2',...]
     * @return void
     */
    public function update(int $id, array $data)
    {
        $this->model->where('id', $id)->update($data);

        return $this->getOne(['id' => ['=', $id]]);
    }

    /**
     * Undocumented function
     *
     * @param array $params ['field_name' => ['operator', 'value]] (example: ['id'=>['=',1]])
     * @param array $data  ['colName'=>'value','colName2'=>'value2',...]
     * @return void
     */
    public function updateByParams(array $params, array $data)
    {
        $query = $this->model->where(function($query) use ($params) {
            $this->where($query, $params);
        });
        $query->update($data);

        return $this->getOne($params);
    }

    /**
     * Delete data
     *
     * @param integer|array $ids
     * @return void
     */
    public function delete(int|array $ids)
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        $this->model->destroy($ids);

        return true;
    }

    /**
     * Restore data
     *
     * @param integer|array $ids
     * @return void
     */
    public function restore(int|array $ids)
    {
        $this->model->withTrashed()->whereIn('id', $ids)->restore();

        return true;
    }

    /**
     * Where arguments
     *
     * @param [type] $query
     * @param [type] $params ['field_name' => ['operator', 'value]] (example: ['id'=>['=',1]])
     * @return void
     */
    public function where(&$query, $params)
    {
        foreach ($params as $colName => $args) {
            list($operator, $value) = $args;
            if (!empty($value)) {
                $query->where($colName, $operator, $value);
                # code...
            }
        }
    }

    /**
     * For Customer API
     *
     * @return void
     */
    public function forCustomer()
    {
        $this->customer = true;

        return $this;
    }

    public function getModelConstantValue(string $name, $prefix = null)
    {
        $constant = strtoupper($name);

        if ($prefix) {
            $constant = $prefix.$constant;
        }

        return constant($this->model::class.'::'.$constant);
    }
}
