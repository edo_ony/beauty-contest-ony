<?php
namespace App\Repositories;

use App\Models\Vehicle;

class VehicleRepository extends BaseRepository
{
    protected function makeModel()
    {
        return new Vehicle;
    }

}
