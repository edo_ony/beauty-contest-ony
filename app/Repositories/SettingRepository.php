<?php
namespace App\Repositories;

use App\Models\Settings;

class SettingRepository extends BaseRepository
{
    protected function makeModel()
    {
        return new Settings;
    }

    public function updateByName($name, $value)
    {
        $setting = $this->getOne(['name' => ['=',$name]]);
        if ($setting) {
            $this->update($setting->id, ['value'=>$value]);
        } else {
            $this->create(['name' => $name, 'value' => $value]);
        }
    }

    public function getAllByPrefix($prefix)
    {
        return $this->getModel()->where('name', 'LIKE', $prefix.'%')->get();
    }
}
