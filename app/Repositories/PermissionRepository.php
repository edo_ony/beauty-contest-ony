<?php
namespace App\Repositories;

use App\Models\Permissions;
use Illuminate\Support\Facades\DB;

class PermissionRepository extends BaseRepository
{
    protected function makeModel()
    {
        return new Permissions;
    }

    public function getPaginate(array $params, $order, $sort)
    {
        $query = $this->getModel()->select(['permissions.*']);

        if (!empty($params['search'])) {
            $query->where(DB::raw('lower(name)'), 'like', '%'.strtolower($params['search']).'%');
        }

        return $query->paginate();
    }
}
