<?php
namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserRepository extends BaseRepository
{
    /**
     * Instantiating model to build query
     *
     * @return void
     */
    protected function makeModel()
    {
        return new User;
    }

    public function getPaginate(array $params, $order, $sort)
    {
        $query = $this->getModel()->select(['users.*']);

        if (!empty($params['search'])) {
            $query->where(DB::raw('lower(name)'), 'like', '%'.strtolower($params['search']).'%');
        }

        return $query->paginate();
    }
}
