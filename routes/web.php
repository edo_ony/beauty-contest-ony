<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ParkingRecordController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserRoleController;
use App\Http\Controllers\GuestController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('guest', [GuestController::class, 'index'])->name('guest.index');

Route::get('ajax/getSlot', [ParkingRecordController::class, 'getSlot'])->name('guests.slot');

Route::middleware('guest')->group(function() {
    Route::prefix('/login')->group(function() {
        Route::get('/', [AuthController::class, 'login'])->name('login.index');
        Route::post('/', [AuthController::class, 'storeLogin'])->name('login.store');
    });
});

Route::middleware('auth')->group(function(){
    Route::get('/', [HomeController::class, 'index'])->name('dashboard');
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

    Route::prefix('user')->group(function(){
        Route::get('/', [UserController::class, 'index'])->name('user.index')->can('user');
        Route::post('/bulk/{type}', [UserController::class, 'bulkAction'])->name('user.bulk')->where('type','delete')->can('user');
        Route::post('/{id}/delete', [UserController::class, 'delete'])->name('user.destroy')->can('user');
        Route::post('/{id}/update', [UserController::class, 'update'])->name('user.update')->can('user');
        Route::get('/{id}/edit', [UserController::class, 'edit'])->name('user.edit')->can('user');
        Route::post('/store', [UserController::class, 'store'])->name('user.store')->can('user');
        Route::get('/create', [UserController::class, 'create'])->name('user.create')->can('user');
    });

    Route::prefix('role')->group(function(){
        Route::get('/', [UserRoleController::class, 'index'])->name('role.index')->can('role');
        Route::post('/{id}/delete', [UserRoleController::class, 'delete'])->name('role.destroy')->can('role');
        Route::post('/{id}/update', [UserRoleController::class, 'update'])->name('role.update')->can('role');
        Route::get('/{id}/edit', [UserRoleController::class, 'edit'])->name('role.edit')->can('role');
        Route::post('/store', [UserRoleController::class, 'store'])->name('role.store')->can('role');
        Route::get('/create', [UserRoleController::class, 'create'])->name('role.create')->can('role');
    });

    Route::prefix('parking')->group(function(){
        Route::get('/', [ParkingRecordController::class, 'index'])->name('parking.index')->can('parking');
        Route::get('/{mode}', [ParkingRecordController::class, 'indexMode'])->name('parking.mode')->where('type','checkin|checkout')->can('parking');
        Route::get('/{mode}/{type}', [ParkingRecordController::class, 'indexType'])->name('parking.type')->where('type','r2|r4')->can('parking');
        Route::post('/{type}/checkin', [ParkingRecordController::class, 'storeCheckin'])->name('parking.checkin')->where('type','r2|r4')->can('parking');
        Route::post('/{type}/checkout', [ParkingRecordController::class, 'storeCheckout'])->name('parking.checkout')->where('type','r2|r4')->can('parking');
    });

    Route::prefix('settings')->group(function(){
        Route::get('/', [SettingController::class, 'index'])->name('settings.index')->can('settings');
        Route::post('/update', [SettingController::class, 'update'])->name('settings.update')->can('save.settings');
    });

    Route::prefix('notifications')->group(function(){
        Route::get('/', [NotificationController::class, 'index'])->name('notif.index');
        Route::post('push', [NotificationController::class, 'store'])->name('notif.store');
    });
});
